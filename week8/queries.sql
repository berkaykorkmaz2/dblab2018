use company;

update Customers
set ContactName = "Berkay Korkmaz"
where CustomerID = 3;

update Customers
set ContactName = "Berkay Korkmaz", City = "Mugla"
where Country = "Argentina";

select OrderID
from Orders
where CustomerID = 3;

delete from orderdetails
where OrderID = 10365;

delete from Orders
where CustomerID=3;

delete from customers
where CustomerID=3;

select city from Customers
union
select city from Suppliers;

