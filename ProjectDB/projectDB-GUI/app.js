const express = require('express');
const mysql = require('mysql');
const app = express();
app.set('view engine', 'jade');
app.use(express.static(__dirname + '/static'));

// Create Database Connection

const db = mysql.createConnection({
    host     : 'localhost',
    user     : 'root',
    password : '1',
    database : 'usedcars_db'
});

db.connect((err) => {
    if(err){
        console.log('Cannot Connect to MySql');
        throw err;
    }else{
        console.log('MySql Connected...');
    }
});


let getSql = function(page){

    if (page > 1){
        return 'select * from GeneralView limit ' + ((page - 1) * 10) + ', 10;';
    }
    return 'select * from GeneralView limit 10;';
};

app.get('', (req, res) => {
    db.query(getSql(0), (error, results) => {
        if (error) throw error;
        res.render('index', {
            title: 'Used Cars - Database',
            data: results
        });
    });
});

app.get(':page', (req, res) => {
    console.log("/:page");
    db.query(getSql(parseInt(req.params['page'])), (error, results) => {
        if (error) throw error;
        res.render('index', {
            title: "Used Cars - Database",
            data: results
        });
    });
});

app.get('/detail/:id', (req, res) => {
        console.log("/detail");
    
        db.query('select * from GeneralView where id = ?', [req.params['id']], (error, results) => {
            if (error) throw error;
            res.render('detail', {
                title: results[0]['name'],
                data: results
            });
        });
});

app.get('/search', (req, res) => {
    let sql = "call SearchGeneralView(?, ?)";
    let p = req.param('p');
    if(p==undefined)
        p = 10
    else
        p = p *10
    db.query(sql, [req.param('q'), p], (error, results) => {
        if (error) throw error;
        res.render('index', {
            title: req.param('q'),
            data: results[0]
        });
    });
});





app.listen('3000', () => {
    console.log('Server started on port 3000...')
});