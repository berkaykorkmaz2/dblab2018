function nextPage() {


    if (String(window.location.href).includes("search?q=")) {

        var url = window.location.href.split('=');
        var page = url.pop()
        window.location.href = url.join('=') + '=' + (++page);

    } else {
        var url = window.location.href.split('/');

        var page = parseInt(url.pop());

        if (isNaN(page)) {
            page = 1;
        }
        window.location.href = url.join('/') + '/' + (++page);
    }

}

function prevPage() {

    if (String(window.location.href).includes("search?q=")) {
        var url = window.location.href.split('=');
        var page = url.pop()
        if(page == 1) {
            page = 2;
        }
        window.location.href = url.join('=') + '=' + (--page);
    } else {
        var url = window.location.href.split('/'),
            page = parseInt(url.pop());
        if (page == 2 || isNaN(page)) {
            window.location.href = url.join('/');
        } else {
            window.location.href = url.join('/') + '/' + (--page);
        }
    }
}