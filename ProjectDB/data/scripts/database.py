import MySQLdb
import csv

conn = MySQLdb.connect(host= "localhost",user="root", passwd="1",db="usedcars_db")
x = conn.cursor()


abtestDic = {}
gearboxDic = {}
vehicleDic = {}
fuelDic = {}
placeDic = {}
modelDic = {}



fks = {3 : ["abtest","type"],6 : ["gearbox","type"],4 : ["vehicleType","type"],11 : ["fuelType","type"],15 : ["place","postal_code"],8 : ["model","name"]}

def getFK(col,val,deff):

    if(col == 3):
        if val in abtestDic.keys():
            return abtestDic[val]
        else:
            sql = "select * from "+fks[col][0]+ " where "+fks[col][1]+" = "+" '"+str(val)+"'"
            x.execute(sql)
            # Fetch all the rows in a list of lists.
            results = x.fetchall()
            if(len(results)>0):
                abtestDic[val] = results[0][0]
                return results[0][0]
            abtestDic[val] = deff
            return deff
    if(col == 6):
        if val in gearboxDic.keys():
            return gearboxDic[val]
        else:
            sql = "select * from "+fks[col][0]+ " where "+fks[col][1]+" = "+" '"+str(val)+"'"
            x.execute(sql)
            # Fetch all the rows in a list of lists.
            results = x.fetchall()
            if(len(results)>0):
                gearboxDic[val] = results[0][0]
                return results[0][0]
            gearboxDic[val] = deff
            return deff
    if(col == 4):
        if val in vehicleDic.keys():
            return vehicleDic[val]
        else:
            sql = "select * from "+fks[col][0]+ " where "+fks[col][1]+" = "+" '"+str(val)+"'"
            x.execute(sql)
            # Fetch all the rows in a list of lists.
            results = x.fetchall()
            if(len(results)>0):
                vehicleDic[val] = results[0][0]
                return results[0][0]
            vehicleDic[val] = deff
            return deff
    if(col == 11):
        if val in fuelDic.keys():
            return fuelDic[val]
        else:
            sql = "select * from "+fks[col][0]+ " where "+fks[col][1]+" = "+" '"+str(val)+"'"
            x.execute(sql)
            # Fetch all the rows in a list of lists.
            results = x.fetchall()
            if(len(results)>0):
                fuelDic[val] = results[0][0]
                return results[0][0]
            fuelDic[val] = deff
            return deff
    if(col == 15):
        if val in placeDic.keys():
            return placeDic[val]
        else:
            sql = "select * from "+fks[col][0]+ " where "+fks[col][1]+" = "+" '"+str(val)+"'"
            x.execute(sql)
            # Fetch all the rows in a list of lists.
            results = x.fetchall()
            if(len(results)>0):
                placeDic[val] = results[0][0]
                return results[0][0]
            placeDic[val] = deff
            return deff
    if(col == 8):
        if val in modelDic.keys():
            return modelDic[val]
        else:
            sql = "select * from "+fks[col][0]+ " where "+fks[col][1]+" = "+" '"+str(val)+"'"
            x.execute(sql)
            # Fetch all the rows in a list of lists.
            results = x.fetchall()
            if(len(results)>0):
                modelDic[val] = results[0][0]
                return results[0][0]
            modelDic[val] = deff
            return deff



def newLine():
    line = "{}," * 13
    line = line[0:len(line) - 1]
    return line

line = newLine()
max = 0
counter = 0
maxcounter = 0
ocline = ""
with open('autos2.csv', 'r', encoding = "ISO-8859-15") as file, open('advert.csv', 'w', encoding = "ISO-8859-15") as out:
    readCSV = csv.reader(file, delimiter=',')
    mylines = []
    for row in readCSV:
        repaired = ""
        if(row[13] == "ja"):
            repaired = "1"
        elif(row[13] == "nein"):
            repaired = "0"

        line = line.format(row[1].replace(',',''),row[2].replace(',',''),row[7].replace(',',''),row[9].replace(',',''),repaired,row[14].replace(',',''),row[0].replace(',',''),getFK(3,row[3],3),getFK(6,row[6],3),getFK(4,row[4],1),getFK(11,row[11],8),getFK(15,row[15],84489),getFK(8,row[8],1))
        maxcounter = maxcounter + 1
        if(maxcounter%10000 == 0):
            print("10k")
                    
        if maxcounter != 1:
            line = line.replace(';','')
            if(len(line.replace(',',''))>0 and len(row[6])>0  ):
                mylines.append(line)
                
            counter = counter + 1
        line = newLine()

    for line in set(mylines):
        out.write(line + '\n')
# dateCrawled,0
# name,1
# price,2
# abtest,3
# vehicleType,4
# yearOfRegistration,5
# gearbox,6
# powerPS,7
# model,8
# kilometer,9
# monthOfRegistration,10
# fuelType,11
# brand,12
# notRepairedDamage,13
# dateCreated,14
# postalCode 15
print(counter)
print(counter-maxcounter)
print(maxcounter)



# Postal Code,Place Name,State,State Abbreviation,City,Latitude,Longitude,
