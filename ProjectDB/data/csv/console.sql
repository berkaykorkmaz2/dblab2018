# ALTER TABLE state AUTO_INCREMENT = 1

# select b.name, count(ad.id) as salescount from advert as ad
# join brand as b on ad.model_id = b.id
# group by b.name order by salescount limit 1

# select b.name, m.name, ad.price from advert as ad
# join model as m on ad.model_id = m.id
# join brand as b on b.id = m.brand_id
#   where m.year = 2009
# order by ad.price desc limit 10

# select m.name, count(ad.id) as salescount from advert as ad
# join model as m on ad.model_id = m.id
# join brand as b on b.id = m.brand_id
# where b.name = "bmw"
# group by m.name
# order by salescount asc limit 5

# select b.name, avg(ad.kilometer) as Average from advert as ad
# join model as m on ad.model_id = m.id
# join brand as b on b.id = m.brand_id
# where b.name="audi" and m.year = 1988
# group by b.name

# select vt.type as type, count(ad.id) as salescount from advert as ad
# join vehicleType vt on ad.vehicleType_id = vt.id
# join model as m on ad.model_id = m.id
# join brand as b on b.id = m.brand_id
# where b.name= "ford"
# group by  type
# order by salescount limit 1