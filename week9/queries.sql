SELECT count(customers.CustomerID) AS numberOfCustomers,
       customers.Country
FROM customers
GROUP BY customers.Country
ORDER BY numberOfCustomers DESC;


SELECT count(products.productID) AS numberOfProducts,
       suppliers.SupplierName
FROM products
JOIN suppliers ON products.SupplierID = suppliers.SupplierID
GROUP BY products.SupplierID
ORDER BY numberOfProducts DESC;


SELECT *
FROM customers;

SHOW variables LIKE '--secure_file_priv';

LOAD DATA infile "employees.csv" INTO TABLE employees fields terminated BY ','
IGNORE 1 lines;

LOAD DATA infile "customers.csv" INTO TABLE customers fields terminated BY ';'
IGNORE 1 lines;

LOAD DATA infile "categories.tsv" INTO TABLE categories;
