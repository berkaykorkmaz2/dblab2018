-- create index abc on Customers(CustomerName);
-- explain select * from Customers order by CustomerName;
-- alter table Customers drop index abc;
-- create view myView as select * from Customers order by CustomerName;
-- select * from myView;
-- insert into myView values (92, 'John Doe', 'Doe John', 'Kotekli', 'new york', '40900', 'Japan');
select * from myView;